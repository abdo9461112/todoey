//
//  AppDelegate.swift
//  Destini
//
//  Created by Philipp Muellauer on 01/09/2015.
//  Copyright (c) 2015 London App Brewery. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        if let realmURL = Realm.Configuration.defaultConfiguration.fileURL {
//            let realmURLs = [
//                realmURL,
//                realmURL.appendingPathExtension("lock"),
////                realmURL.appendingPathExtension("note"),
////            ]
////
////            for url in realmURLs {
//                do {
//                    try FileManager.default.removeItem(at: url)
//                } catch {
//                    // Handle error
//                    print("Error deleting Realm file: \(error)")
//                }
//            }
//        }
        do {
            _ = try Realm()
            
        }catch {
            print("something went wrong with realm\(error)")
        }
        return true
    }
    
}
