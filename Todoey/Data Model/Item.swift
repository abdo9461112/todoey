//
//  Item.swift
//  Todoey
//
//  Created by abdo emad  on 24/11/2023.
//  Copyright © 2023 App Brewery. All rights reserved.
//

import Foundation
import RealmSwift

class Item : Object{
    @objc dynamic var title : String = ""
    @objc dynamic var done : Bool = false
    @objc dynamic var dateCreated :Date?
    var parentCategor = LinkingObjects(fromType: Category.self, property: "itemss")

}
