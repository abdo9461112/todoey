//
//  ToDoListTableViewController.swift
//  Todoey
//
//  Created by abdo emad  on 24/11/2023.
//  Copyright © 2023 App Brewery. All rights reserved.
//

import UIKit
import RealmSwift

class ToDoListTableViewController: UITableViewController {

    let realm = try! Realm()
    var items : Results<Item>?
    
    var selectedCategory : Category?{
        didSet{
            loadItems()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = selectedCategory?.nam
       
       
    }

    // MARK: - Table view data source

   
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items?.count ?? 1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if let item = items?[indexPath.row]{
            
            cell.textLabel?.text = item.title
            cell.accessoryType = item.done ? .checkmark : .none
        }else{
            cell.textLabel?.text = "no items add"
        }
        
        return cell
    }
    func save(item : Item){
        do {
            try realm.write{
                realm.add(item)
            }
        }catch{
            print("something went wrong with saving \(error)")
            
        }
        tableView.reloadData()
    }
    
    func loadItems(){
        items = selectedCategory?.itemss.sorted(byKeyPath: "title", ascending: true)
        tableView.reloadData()
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let item = items?[indexPath.row]{
            do{
                try realm.write {
                    item.done = !item.done
                }
            }catch{
                print("error of saving done statas")
            }
        }
        tableView.reloadData()
        tableView.deselectRow(at: indexPath, animated: true)
    }

    @IBAction func addButtonPressed(_ sender: Any) {
        var textField = UITextField()
        
        
        let alert = UIAlertController(title: "Add new item", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        let action = UIAlertAction(title: "Add", style: .default){ (action) in
            
            if let currentCategory = self.selectedCategory{
                do {
                    try self.realm.write {
                        let newItem = Item()
                        newItem.title = textField.text!
                        newItem.dateCreated = Date()
                        currentCategory.itemss.append(newItem)
                    }
                }catch{
                    print("error saving items \(error)")
                }
                
            }
            self.tableView.reloadData()
            
        }
        alert.addAction(action)
        alert.addTextField { (field) in
            textField = field
            textField.text = "NEW TASK"
        }
        present(alert , animated: true, completion: nil)
    }
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: "delete") { ( action , view, completionHandler) in
            if let itemDeletion =  self.selectedCategory?.itemss[indexPath.row]{
                do {
                    try self.realm.write {
                        self.realm.delete(itemDeletion)
                    }
                }catch{
                    
                }
                tableView.reloadData()

            }
            

            completionHandler(true)
        }
        
        let favoriteAction = UIContextualAction(style:.normal , title: "favorite" ) { (_, _, _) in
            print ("user add")
            
        }
        favoriteAction.image = UIImage(systemName: "heart")
        deleteAction.image = UIImage(systemName: "trash")
        favoriteAction.backgroundColor = UIColor(named: "red")
        
        return UISwipeActionsConfiguration(actions: [deleteAction ,favoriteAction])
    }
    
    
    
    
}
extension ToDoListTableViewController : UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        items = items?.filter("title CONTAINS[cd] %@", searchBar.text!).sorted(byKeyPath: "dateCreated", ascending: true)
        tableView.reloadData()
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            loadItems()
            
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
        }
    }
}
