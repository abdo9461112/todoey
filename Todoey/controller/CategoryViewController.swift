//
//  CategoryViewController.swift
//  Todoey
//
//  Created by abdo emad  on 24/11/2023.
//  Copyright © 2023 App Brewery. All rights reserved.
//

import UIKit
import RealmSwift

class CategoryViewController: UITableViewController {

    let realm = try! Realm()
    var categories : Results<Category>?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadCategoies()
        
       
    }
    
    

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return categories?.count ?? 1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = categories?[indexPath.row].nam ?? "no categories added yet"
        return cell
    }
    // MARK: - Table view delgete mathods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToDoList", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVc = segue.destination as! ToDoListTableViewController
        if let indexPath = tableView.indexPathForSelectedRow{
            destinationVc.selectedCategory = categories?[indexPath.row]
        }
    }

    func save(category : Category){
        do {
            try realm.write{
                realm.add(category)
            }
        }catch{
            print("something went wrong with saving \(error)")
            
        }
        tableView.reloadData()
    }
    func loadCategoies(){
        categories = realm.objects(Category.self)
        tableView.reloadData()
    }
    
    @IBAction func addButtnPressed(_ sender: UIBarButtonItem) {
        
        var textField = UITextField()
        let alert = UIAlertController(title: "Add New Category", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        let action = UIAlertAction(title: "Add", style: .default) { (actoin) in
            
            let newCatgory = Category()
            newCatgory.nam = textField.text!
            
            self.save(category: newCatgory)
        }
        alert.addAction(action)
        alert.addTextField { (field) in
            textField = field
            textField.placeholder = "Add a new catgory"
        }
        present(alert, animated: true, completion: nil)
    }
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
    let deleteAction = UIContextualAction(style: .destructive, title: "delete") { ( action , view, completionHandler) in
        if let categoryDeletion =  self.categories?[indexPath.row]{
            do {
                try self.realm.write {
                    self.realm.delete(categoryDeletion)
                }
            }catch{
                
            }
            tableView.reloadData()
        }
        
    
        completionHandler(true)
    }
    
    let favoriteAction = UIContextualAction(style:.normal , title: "favorite" ) { (_, _, _) in
        print ("user add")
        
    }
    favoriteAction.image = UIImage(systemName: "heart")
    deleteAction.image = UIImage(systemName: "trash")
    favoriteAction.backgroundColor = UIColor(named: "red")
    
    return UISwipeActionsConfiguration(actions: [deleteAction ,favoriteAction])
}

}
